package de.sachsbauer.codecube.labyrinth;

import javafx.animation.PauseTransition;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import de.sachsbauer.codecube.codeparts.Direction;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller des Labyrinthes. Kommunikation mit Model und View per Attribut bzw. FXML
 * @author Daniel Bauersachs
 */
public class Controller implements Initializable {
    private final Model model;

    @FXML
    private VBox vBoxGrid;

    @FXML
    private Button resetButton;

    @FXML
    private Text title, codeText;

    @FXML
    private AnchorPane anchor;

    @FXML
    private Circle solvedLight;

    private Shape cursor;

    public Controller(Model model) {
        this.model = model;
    }

    /**
     * Wird nach dem Konstruieren des Controllers aufgerufen und erstelllt die Stackpanes, den Cursor und
     * registriert die {@link javafx.event.EventHandler} für Maus und Tastatur.
     * @param location Nicht genutzt
     * @param resources Nicht genutzt
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialisieren des Titels
        title.textProperty().bind(model.title());

        // Initialisieren der Anzeige des Codes
        codeText.setText(model.getCodeTree().toString());

        // Initialisieren der Anzeige des Spielfelds
        for (int i = 0; i < model.getHeight(); i++) {
            HBox hBox = new HBox();
            hBox.getStyleClass().add("hBox");
            vBoxGrid.getChildren().add(hBox);
            for (int k = 0; k < model.getWidth(); k++) {
                Shape shape = createShape();
                shape.fillProperty().setValue(model.getGroundAt(k, i));
                StackPane stackPane = new StackPane(shape);
                ((HBox) vBoxGrid.getChildren().get(i)).getChildren().add(stackPane);
            }
        }

        initCursor();
        model.wrongDirection().addListener(this :: wrongDirection);

        resetButton.setOnAction(this :: resetButtonPressed);
        resetButton.disableProperty().bind(model.uiLocked());

        initUserControl();

        addChangeListener();
    }

    private void wrongDirection(Observable observable, Boolean aBoolean, Boolean t1) {
        if(t1) {
            model.uiLocked().set(true);
            cursor.setFill(Color.RED);

            PauseTransition pause = new PauseTransition(Duration.millis(600));
            pause.setOnFinished(e -> {
                cursor.setFill(Color.BLACK);
                model.wrongDirection().set(false);
                model.uiLocked().set(false);
            });
            pause.play();
        }
    }


    private void initCursor() {
        cursor = new Circle(100/(double) calculateMaxDimension());
        cursor.setFill(Color.BLACK);
        model.cursorX().addListener(this :: cursorMoved);
        model.cursorY().addListener(this :: cursorMoved);
        drawCursor(model.cursorX().get(), model.cursorY().get());
    }

    private void cursorMoved(Observable observable) {
        StackPane stackPane = (StackPane) cursor.getParent();
        stackPane.getChildren().remove(cursor);
        drawCursor(model.cursorX().get(), model.cursorY().get());
    }

    private void drawCursor(int x, int y) {
        ((StackPane)((HBox)vBoxGrid.getChildren().get(y)).getChildren().get(x)).getChildren().add(cursor);
    }

    private void resetButtonPressed (ActionEvent event) {
        model.reset();
    }

    private Shape createShape() {
        return new Circle(300/(double)calculateMaxDimension());
    }

    private int calculateMaxDimension() {
        return Math.max(model.getHeight(), model.getWidth());
    }

    private void initUserControl() {
        anchor.addEventHandler(MouseEvent.MOUSE_ENTERED, event -> {
            if(model.uiLocked().get()) return;

            anchor.requestFocus();
        });
        anchor.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if(model.uiLocked().get()) return;

            switch (event.getCode()) {
                case W: case UP:
                    model.tryMoveCursor(Direction.UP);
                    break;
                case S: case DOWN:
                    model.tryMoveCursor(Direction.DOWN);
                    break;
                case A: case LEFT:
                    model.tryMoveCursor(Direction.LEFT);
                    break;
                case D: case RIGHT:
                    model.tryMoveCursor(Direction.RIGHT);
                    break;
            }
        });
    }

    /**
     * Fügt einen ChangeListener für die Lampen hinzu.
     * Dieser sorgt dafür, dass die Lampen rot bei ungelöstem und grün bei gelöstem Rätsel sind.
     */
    private void addChangeListener () {
        BooleanProperty b = model.solved();
        b.addListener( (observableValue, aBoolean, t1) ->
                solvedLight.setStyle("-fx-fill: " + (t1 ? "green" : "red")));
    }
}