package de.sachsbauer.codecube.labyrinth;

import de.sachsbauer.codecube.codeparts.CodeModel;
import de.sachsbauer.codecube.codeparts.CodeNode;
import de.sachsbauer.codecube.codeparts.Direction;
import javafx.scene.paint.Color;
import nu.xom.Element;

import java.util.ArrayList;

public class Model extends CodeModel {

    public Model() {
        super();
    }

    @Override
    public void initModel(Element root) {
        for(Element el : root.getChildElements()) {
            String elementName = el.getQualifiedName();
            switch(elementName) {
                case "puzzleTitle":
                    title().set(el.getValue());
                    break;
                case "width":
                    setWidth(Integer.parseInt(el.getValue()));
                    break;
                case "height":
                    setHeight(Integer.parseInt(el.getValue()));
                    for (int i = 0; i < getHeight(); i++) {
                        getBoard().add(new ArrayList<>());
                        for (int k = 0; k < getWidth(); k++) {
                            getBoard().get(i).add(Color.GREY);
                        }
                    }
                    break;
                case "ground":
                    int x = Integer.parseInt(el.getFirstChildElement("X").getValue());
                    int y = Integer.parseInt(el.getFirstChildElement("Y").getValue());
                    setGroundAt(x, y, Color.valueOf(el.getFirstChildElement("color").getValue()));
                    break;
                case "code":
                    ArrayList<CodeNode> codeNodes = new ArrayList<>();
                    for(Element childElement : el.getChildElements("codeNode")) {
                        codeNodes.add(buildCodeTree(getCodeTree(), childElement));
                    }
                    getCodeTree().setChildren(codeNodes);
                    setCurrentLeaf(getCodeTree().getNext());
                    break;
                default:
                    System.err.println("Error in Toxicode: " + this + " - Unbekanntes Element.");
                    System.exit(1);
            }
        }
    }

    /**
     * Bewegt nach Korrektheitsprüfung den Cursor auf dem Spielfeld.
     * @param direction Zugrichtung
     */
    public void tryMoveCursor(Direction direction) {
        if(getCurrentLeaf() != null) {
            if(getCurrentLeaf().getDirection() == direction) {
                moveCursor(direction);
                setCurrentLeaf(getCurrentLeaf().getNext());
                if(getCurrentLeaf() == null) {
                    solved().set(true);
                    uiLocked().set(true);
                }
            } else {
                wrongDirection().set(true);
                reset();
            }
        }
    }

    public void reset() {
        getCodeTree().resetNode();
        setCurrentLeaf(getCodeTree().getNext());
        solved().set(false);
        resetCursor();
    }
}