package de.sachsbauer.codecube;

import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class XMLValidator {

    private static XMLValidator instance;

    private XMLValidator() {}

    public static XMLValidator getInstance() {
        if(XMLValidator.instance == null) {
            XMLValidator.instance = new XMLValidator();
        }
        return instance;
    }

    public void validateXMLSchema (URL xsdURL, String xmlPath) throws SAXException, IOException {
        SchemaFactory factory =
                SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = factory.newSchema(xsdURL);
        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(new File(xmlPath)));
    }
}