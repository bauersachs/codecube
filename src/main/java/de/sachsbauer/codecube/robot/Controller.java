package de.sachsbauer.codecube.robot;

import de.sachsbauer.codecube.codeparts.CodeLeaf;
import de.sachsbauer.codecube.codeparts.CodeNode;
import de.sachsbauer.codecube.codeparts.CodeSequence;
import javafx.animation.PauseTransition;
import javafx.beans.Observable;
import javafx.beans.property.BooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable, PropertyChangeListener {
    private final Model model;

    @FXML
    private VBox vBoxGrid;

    @FXML
    private Button resetButton, playButton, playCodeButton;

    @FXML
    private Text title;

    @FXML
    private AnchorPane anchor;

    @FXML
    private Circle solvedLight;

    @FXML
    private TreeView<CodeNode> codeTreeView;

    private Shape cursor;

    public Controller(Model model) {
        this.model = model;
    }

    /**
     * Wird nach dem Konstruieren des Controllers aufgerufen und erstelllt die Stackpanes und den Cursor
     * @param location Nicht genutzt
     * @param resources Nicht genutzt
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialisieren des Titels
        title.textProperty().bind(model.title());

        codeTreeView.setRoot(drawCodeTree(model.getCodeTree()));
        codeTreeView.setCellFactory(new TreeCellFactory(model));
        codeTreeView.disableProperty().bind(model.uiLocked());

        // Initialisieren der Anzeige des Spielfelds
        for (int i = 0; i < model.getHeight(); i++) {
            HBox hBox = new HBox();
            hBox.getStyleClass().add("hBox");
            vBoxGrid.getChildren().add(hBox);
            for (int k = 0; k < model.getWidth(); k++) {
                Shape shape = createShape();
                shape.fillProperty().setValue(model.getGroundAt(k, i));
                StackPane stackPane = new StackPane(shape);
                ((HBox) vBoxGrid.getChildren().get(i)).getChildren().add(stackPane);
            }
        }

        initCursor();
        model.wrongDirection().addListener(this::wrongDirection);

        resetButton.setOnAction(this :: resetButtonPressed);
        resetButton.disableProperty().bind(model.uiLocked());

        playButton.setOnAction(this :: playButtonPressed);
        playButton.disableProperty().bind(model.uiLocked());

        playCodeButton.setOnAction(this :: playCodeButtonPressed);
        playCodeButton.disableProperty().bind(model.uiLocked());

        model.addPropertyChangeListener(this);
        addChangeListener();
        propertyChange(null);
    }

    private void initCursor() {
        cursor = new Circle(100/(double) calculateMaxDimension());
        cursor.setFill(Color.BLACK);
        model.cursorX().addListener(this :: cursorMoved);
        model.cursorY().addListener(this :: cursorMoved);
        drawCursor(model.cursorX().get(), model.cursorY().get());
    }

    private void cursorMoved(Observable observable) {
        StackPane stackPane = (StackPane) cursor.getParent();
        stackPane.getChildren().remove(cursor);
        drawCursor(model.cursorX().get(), model.cursorY().get());
    }

    private void drawCursor(int x, int y) {
        ((StackPane)((HBox)vBoxGrid.getChildren().get(y)).getChildren().get(x)).getChildren().add(cursor);
    }

    private TreeItem<CodeNode> drawCodeTree(CodeNode codeNode) {
        TreeItem<CodeNode> codeTree = null;
        if(codeNode instanceof CodeLeaf) {
            codeTree = new TreeItem<>(codeNode);
        } else if (codeNode instanceof CodeSequence) {
            CodeSequence codeSequence = (CodeSequence)codeNode;
            codeTree = new TreeItem<> (codeSequence);
            codeTree.setExpanded(true);
            for(CodeNode child : codeSequence.getChildren()) {
                codeTree.getChildren().add(drawCodeTree(child));
            }
        }

        return codeTree;
    }

    private void resetButtonPressed (ActionEvent event) {
        model.reset();
    }

    private void playButtonPressed (ActionEvent event) {
        model.playDirections();
    }

    private void playCodeButtonPressed (ActionEvent event) {
        model.playCode();
    }

    private void wrongDirection(Observable observable, Boolean aBoolean, Boolean t1) {
        if(t1) {
            model.uiLocked().set(true);
            cursor.setFill(Color.RED);

            PauseTransition pause = new PauseTransition(Duration.millis(600));
            pause.setOnFinished(e -> {
                cursor.setFill(Color.BLACK);
                model.wrongDirection().set(false);
                model.uiLocked().set(false);
            });
            pause.play();
        }
    }

    private Shape createShape() {
        return new Circle(300/(double)calculateMaxDimension());
    }

    private int calculateMaxDimension() {
        return Math.max(model.getHeight(), model.getWidth());
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        codeTreeView.setRoot(null);
        codeTreeView.setRoot(drawCodeTree(model.getCodeTree()));
        codeTreeView.setCellFactory(new TreeCellFactory(model));
    }

    /**
     * Fügt einen ChangeListener für die Lampen hinzu.
     * Dieser sorgt dafür, dass die Lampen rot bei ungelöstem und grün bei gelöstem Rätsel sind.
     */
    private void addChangeListener () {
        BooleanProperty b = model.solved();
        b.addListener( (observableValue, aBoolean, t1) ->
                solvedLight.setStyle("-fx-fill: " + (t1 ? "green" : "red")));
    }
}
