package de.sachsbauer.codecube.robot;

import de.sachsbauer.codecube.codeparts.CodeNode;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.util.Callback;

public class TreeCellFactory implements Callback<TreeView<CodeNode>, TreeCell<CodeNode>> {
    private TreeItem<CodeNode> draggedItem;
    private final Model model;

    TreeCellFactory(Model model) {
        this.model = model;
    }

    @Override
    public TreeCell<CodeNode> call(TreeView<CodeNode> treeView) {
        TreeCell<CodeNode> cell = new TreeCell<>() {
            @Override
            protected void updateItem(CodeNode item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null) return;

                setText(item.thisToString());
            }
        };
        cell.setOnDragDetected((MouseEvent event) -> dragDetected(event, cell));
        cell.setOnDragOver((DragEvent event) -> dragOver(event, cell));
        cell.setOnDragDropped((DragEvent event) -> dragDropped(event, cell));

        return cell;
    }

    private void dragDetected(MouseEvent event, TreeCell<CodeNode> treeCell) {
        draggedItem = treeCell.getTreeItem();
        if(draggedItem == null) return;

        Dragboard db = treeCell.startDragAndDrop(TransferMode.MOVE);

        ClipboardContent content = new ClipboardContent();
        content.putString(draggedItem.getValue().thisToString());
        db.setContent(content);
        event.consume();
    }

    private void dragOver(DragEvent event, TreeCell<CodeNode> treeCell) {
        TreeItem<CodeNode> dragOverItem = treeCell.getTreeItem();

        if (draggedItem != null && dragOverItem != null && dragOverItem != draggedItem) {
            event.acceptTransferModes(TransferMode.MOVE);
        }
        event.consume();
    }

    private void dragDropped(DragEvent event, TreeCell<CodeNode> treeCell) {
        TreeItem<CodeNode> dragOverItem = treeCell.getTreeItem();

        CodeNode sourceNode = draggedItem.getValue();
        CodeNode targetNode = dragOverItem.getValue();

        model.moveTreeNode(sourceNode, targetNode);
        event.setDropCompleted(true);
    }
}