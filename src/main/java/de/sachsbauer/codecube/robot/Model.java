package de.sachsbauer.codecube.robot;

import de.sachsbauer.codecube.ModelInterface;
import de.sachsbauer.codecube.codeparts.*;
import javafx.animation.PauseTransition;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import nu.xom.Element;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;

public class Model extends CodeModel implements ModelInterface{
    private final ArrayList<Direction> directions;
    private int directionIndex;
    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);

    public Model() {
        super();
        directions = new ArrayList<>();
        directionIndex = 0;
    }

    @Override
    public void initModel(Element root) {
        for(Element el : root.getChildElements()) {
            String elementName = el.getQualifiedName();
            switch (elementName) {
                case "puzzleTitle":
                    title().set(el.getValue());
                    break;
                case "width":
                    setWidth(Integer.parseInt(el.getValue()));
                    break;
                case "height":
                    setHeight(Integer.parseInt(el.getValue()));
                    for (int i = 0; i < getHeight(); i++) {
                        getBoard().add(new ArrayList<>());
                        for (int k = 0; k < getWidth(); k++) {
                            getBoard().get(i).add(Color.GREY);
                        }
                    }
                    break;
                case "ground":
                    int x = Integer.parseInt(el.getFirstChildElement("X").getValue());
                    int y = Integer.parseInt(el.getFirstChildElement("Y").getValue());
                    setGroundAt(x, y, Color.valueOf(el.getFirstChildElement("color").getValue()));
                    break;
                case "directions":
                    directions.add(Direction.valueOf(el.getAttribute(0).getValue().toUpperCase()));
                    break;
                case "code":
                    ArrayList<CodeNode> codeNodes = new ArrayList<>();
                    for(Element childElement : el.getChildElements("codeNode")) {
                        codeNodes.add(buildCodeTree(getCodeTree(), childElement));
                    }
                    getCodeTree().setChildren(codeNodes);
                    reset();
                    break;
                default:
                    System.err.println("Error in Toxicode: " + this + " - Unbekanntes Element.");
                    System.exit(1);
            }
        }
    }

    public void playDirections() {
        if(directionIndex != directions.size()) {
            uiLocked().set(true);
            Direction direction = directions.get(directionIndex++);
            moveCursor(direction);
            PauseTransition pause = new PauseTransition(Duration.millis(400));
            pause.setOnFinished(e -> playDirections());
            pause.play();
        } else {
            uiLocked().set(false);
            directionIndex = 0;
            resetCursor();
        }
    }

    public void playCode() {
        if(directionIndex != directions.size()) {
            uiLocked().set(true);
            Direction direction = directions.get(directionIndex);
            if (getCurrentLeaf() != null && getCurrentLeaf().getDirection() == direction) {
                directionIndex++;
                moveCursor(direction);
                setCurrentLeaf(getCurrentLeaf().getNext());

                PauseTransition pause = new PauseTransition(Duration.millis(400));
                pause.setOnFinished(e -> playCode());
                pause.play();
            } else {
                directionIndex = 0;
                wrongDirection().set(true);
                resetRun();
            }
        } else {
            directionIndex = 0;
            if (getCurrentLeaf() == null) {
                solved().set(true);
            } else {
                wrongDirection().set(true);
                resetRun();
            }
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        changes.addPropertyChangeListener(propertyChangeListener);
    }

    public void moveTreeNode(CodeNode sourceNode, CodeNode targetNode) {
        if(sourceNode instanceof CodeSequence
                && ((CodeSequence)sourceNode).childrenAsList().contains(targetNode))
            return;

        if(sourceNode.getParent() != null) {
            sourceNode.getParent().getChildren().remove(sourceNode);
        }

        if(targetNode instanceof CodeSequence) {
            CodeSequence targetSequence = ((CodeSequence) targetNode);
            if(targetSequence == sourceNode.getParent()) {
                targetSequence.getParent().getChildren().add(0, sourceNode);
                sourceNode.setParent(targetSequence.getParent());
            } else {
                targetSequence.getChildren().add(0, sourceNode);
                sourceNode.setParent(targetSequence);
            }
        } else if (targetNode instanceof CodeLeaf) {
            CodeSequence targetParent = targetNode.getParent();
            targetParent.getChildren().add(targetParent.getChildren().indexOf(targetNode) + 1, sourceNode);
            sourceNode.setParent(targetParent);
        }

        resetRun();
        changes.firePropertyChange("codeArray", null, null);
    }


    public void resetRun() {
        resetCursor();
        getCodeTree().resetNode();
        setCurrentLeaf(getCodeTree().getNext());
    }

    public void reset() {
        ArrayList<CodeNode> children = getCodeTree().childrenAsList();
        for(CodeNode child : children) {
            if(child instanceof CodeSequence) {
                ((CodeSequence) child).getChildren().clear();
            }
        }
        getCodeTree().getChildren().clear();
        for(CodeNode child : children) {
            getCodeTree().getChildren().add(child);
            child.setParent(getCodeTree());
        }
        Collections.shuffle(getCodeTree().getChildren());
        changes.firePropertyChange("codeArray", null, null);
        resetRun();
    }
}