package de.sachsbauer.codecube;

import javafx.beans.property.BooleanProperty;
import nu.xom.Element;

/**
 * Interface für alle Rätselmodels.
 * Sorgt dafür, dass alle Rätsel zwei {@link BooleanProperty}s erhalten, um ihren Status zu beschreiben,
 * und ihre GUI einzufrieren.
 * Die Methode initModel({@link Element root}) sorgt für die Initalisierung anhand eines XML Elementes.
 * @author Daniel Bauersachs
 */
public interface ModelInterface {
    void initModel(Element root);
    BooleanProperty solved();
    BooleanProperty uiLocked();
}