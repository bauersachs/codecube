package de.sachsbauer.codecube.codeparts;

public class CodeCountLoop extends CodeSequence {
    private final int limit;
    private int index;

    public CodeCountLoop (CodeSequence parent, int limit) {
        super(parent);
        this.limit = limit;
        index = 0;
    }

    @Override
    public CodeLeaf getNext() {
        if(index < limit) {
            index++;
            return super.getNext();
        }
        return getParent().getNeighborOf(this);
    }

    @Override
    public CodeLeaf getNeighborOf(CodeNode child) {
        if(getChildren().indexOf(child) + 1 < getChildren().size()) {
            return super.getNeighborOf(child);
        }
        return getNext();
    }

    @Override
    public String toString() {
        return "wiederhole " +
                limit +
                " mal {\n\t" +
                super.toString().replaceAll("\n", "\n\t") +
                "\n}";
    }

    public String thisToString() {
        return "wiederhole " +
                limit +
                " mal";
    }

    @Override
    public void resetNode() {
        super.resetNode();
        index = 0;
    }
}
