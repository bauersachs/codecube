package de.sachsbauer.codecube.codeparts;

public enum Direction {
    LEFT, RIGHT, UP, DOWN;

    @Override
    public String toString() {
        switch (this) {
            case LEFT: return "links();";
            case RIGHT: return "rechts();";
            case UP: return "hoch();";
            case DOWN: default: return "runter();";
        }
    }
}