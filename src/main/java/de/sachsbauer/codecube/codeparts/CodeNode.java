package de.sachsbauer.codecube.codeparts;

public abstract class CodeNode {
    private CodeSequence parent;

    public abstract CodeLeaf getNext();
    public abstract void resetNode();
    public abstract String toString();
    public abstract String thisToString();

    public CodeModel getModel() {
        return parent.getModel();
    }

    public CodeSequence getParent() {
        return parent;
    }

    public void setParent(CodeSequence parent) {
        this.parent = parent;
    }

}