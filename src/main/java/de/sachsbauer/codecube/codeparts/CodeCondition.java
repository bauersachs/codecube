package de.sachsbauer.codecube.codeparts;

import javafx.scene.paint.Color;

public class CodeCondition extends CodeSequence {
    private final boolean negation;
    private final Color color;

    public CodeCondition (CodeSequence parent, boolean negation, Color color) {
        super(parent);
        this.negation = negation;
        this.color = color;
    }

    @Override
    public CodeLeaf getNext() {
        if(checkCondition()) {
            return super.getNext();
        }
        return getParent().getNeighborOf(this);
    }

    @Override
    public String toString() {
        return "wenn (" +
                (negation ? "!" : "") +
                withColor(color) +
                ") {\n\t" +
                super.toString().replaceAll("\n", "\n\t") +
                "\n}";
    }

    public String thisToString() {
        return "wenn (" +
                (negation ? "!" : "") +
                withColor(color) +
                ")";
    }

    private boolean checkCondition() {
        return negation ^ (getModel().getCurrentGround() == color);
    }
}
