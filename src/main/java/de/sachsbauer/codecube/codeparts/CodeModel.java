package de.sachsbauer.codecube.codeparts;

import de.sachsbauer.codecube.ModelInterface;
import javafx.beans.property.*;
import javafx.scene.paint.Color;
import nu.xom.Element;

import java.util.ArrayList;

/**
 * Dient als Oberklasse für Models, die die Codebaumstruktur und das Spielfeld verwenden
 * @author Daniel Bauersachs
 */
public abstract class CodeModel implements ModelInterface {
    private final StringProperty title;
    private final ArrayList<ArrayList<Color>> board;

    private final BooleanProperty solved;
    private final BooleanProperty uiLocked;
    private final BooleanProperty wrongDirection;

    private final CodeRoot codeTree;
    private CodeLeaf currentLeaf;

    private final IntegerProperty cursorX;
    private final IntegerProperty cursorY;

    private int height;
    private int width;

    public CodeModel () {
        title = new SimpleStringProperty();
        board = new ArrayList<>();
        solved = new SimpleBooleanProperty(false);
        uiLocked = new SimpleBooleanProperty(false);
        wrongDirection = new SimpleBooleanProperty(false);
        codeTree = new CodeRoot(this);
        cursorX = new SimpleIntegerProperty();
        cursorY = new SimpleIntegerProperty();
    }

    /**
     * Baut einen Baum aus CodeNodes aus einem XML-Baum
     * @param current Aktueller CodeNode
     * @param el Abzuarbeitender XML-Baum
     * @return CodeNode-Baum
     */
    public CodeNode buildCodeTree(CodeSequence current, Element el) {
        String nodeType = el.getAttribute(0).getValue();
        switch (nodeType) {
            case "left": case "right": case "up": case "down":
                return new CodeLeaf(current, Direction.valueOf(nodeType.toUpperCase()));
        }

        CodeSequence codeSequence = null;
        switch (nodeType) {
            case "codeCondition":
                codeSequence = new CodeCondition(current,
                        Boolean.parseBoolean(el.getFirstChildElement("negation").getValue()),
                        Color.valueOf(el.getFirstChildElement("color").getValue()));
                break;
            case "codeCountLoop":
                codeSequence = new CodeCountLoop(current, Integer.parseInt(el.getFirstChildElement("limit").getValue()));
                break;
            case "codeConditionLoop":
                codeSequence = new CodeConditionLoop(current,
                        Boolean.parseBoolean(el.getFirstChildElement("negation").getValue()),
                        Color.valueOf(el.getFirstChildElement("color").getValue()));
                break;
        }
        if(codeSequence != null) {
            ArrayList<CodeNode> codeNodes = new ArrayList<>();
            for (Element childElement : el.getChildElements("codeNode")) {
                codeNodes.add(buildCodeTree(codeSequence, childElement));
            }
            codeSequence.setChildren(codeNodes);
            return codeSequence;
        }

        return null;
    }

    public void moveCursor(Direction direction) {
        switch (direction) {
            case UP:
                cursorY().set(Math.floorMod(getCursorY() - 1, getHeight()));
                break;
            case DOWN:
                cursorY().set(Math.floorMod(getCursorY() + 1, getHeight()));
                break;
            case LEFT:
                cursorX().set(Math.floorMod(getCursorX() - 1, getWidth()));
                break;
            case RIGHT:
                cursorX().set(Math.floorMod(getCursorX() + 1, getWidth()));
                break;
        }
    }

    public void resetCursor() {
        cursorX.set(0);
        cursorY.set(0);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public IntegerProperty cursorX() {
        return cursorX;
    }

    public IntegerProperty cursorY() {
        return cursorY;
    }

    public int getCursorX() {
        return cursorX.get();
    }

    public int getCursorY() {
        return cursorY.get();
    }

    @Override
    public BooleanProperty solved() {
        return solved;
    }

    public StringProperty title() {
        return title;
    }

    public ArrayList<ArrayList<Color>> getBoard() {
        return board;
    }

    @Override
    public BooleanProperty uiLocked() {
        return uiLocked;
    }

    public BooleanProperty wrongDirection() {
        return wrongDirection;
    }

    public CodeRoot getCodeTree() {
        return codeTree;
    }

    public CodeLeaf getCurrentLeaf() {
        return currentLeaf;
    }

    public void setCurrentLeaf(CodeLeaf currentLeaf) {
        this.currentLeaf = currentLeaf;
    }

    public Color getCurrentGround() {
        return getGroundAt(getCursorX(), getCursorY());
    }

    public Color getGroundAt(int x, int y) {
        if(y >= 0 && y < getHeight() && x >= 0 && x < getWidth())
            return getBoard().get(y).get(x);
        System.err.println("Feld existiert nicht!");
        return null;
    }

    public void setGroundAt(int x, int y, Color ground) {
        if (y >= 0 && y < getHeight() && x >= 0 && x < getWidth()) {
            getBoard().get(y).set(x, ground);
        } else System.err.println("Feld existiert nicht!");
    }
}