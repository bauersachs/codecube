package de.sachsbauer.codecube.codeparts;

public class CodeRoot extends CodeSequence {
    private final CodeModel model;

    public CodeRoot (CodeModel model) {
        super(null);
        this.model = model;
    }

    @Override
    public CodeModel getModel() {
        return model;
    }

    @Override
    public String thisToString() {
        return "WURZEL";
    }

    @Override
    public CodeLeaf getNext() {
        if(getChildren().size() > 0) {
            return super.getNext();
        }
        return null;
    }

    @Override
    public CodeLeaf getNeighborOf(CodeNode child) {
        if(getChildren().indexOf(child) + 1 < getChildren().size()) {
            return super.getNeighborOf(child);
        }
        return null;
    }
}