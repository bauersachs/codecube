package de.sachsbauer.codecube.codeparts;

import javafx.scene.paint.Color;

import java.util.ArrayList;

public abstract class CodeSequence extends CodeNode {
    private ArrayList<CodeNode> children;

    public CodeSequence (CodeSequence parent) {
        setParent(parent);
        this.children = null;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (CodeNode child : children) {
            stringBuilder.append(child.toString())
                    .append("\n");
        }
        if(stringBuilder.length() > 0)
            stringBuilder.deleteCharAt(stringBuilder.length()-1);
        return stringBuilder.toString();
    }

    public abstract String thisToString();

    @Override
    public CodeLeaf getNext() {
        if(children.size() > 0) {
            if(children.get(0) instanceof CodeLeaf)
                return (CodeLeaf)children.get(0);
            return children.get(0).getNext();
        }
        return getParent().getNeighborOf(this);
    }

    public CodeLeaf getNeighborOf(CodeNode child) {
        if(children.indexOf(child) + 1 < children.size()) {
            if(children.get(children.indexOf(child) + 1) instanceof CodeLeaf)
                return (CodeLeaf)children.get(children.indexOf(child) + 1);
            return children.get(children.indexOf(child) + 1).getNext();
        }
        return getParent().getNeighborOf(this);
    }

    @Override
    public void resetNode() {
        if(children.size() > 0)
            for (CodeNode child : children) child.resetNode();
    }

    public ArrayList<CodeNode> childrenAsList() {
        ArrayList<CodeNode> childrenList = new ArrayList<>(children);
        for (CodeNode child : children) {
            if(child instanceof CodeSequence) {
                childrenList.addAll(((CodeSequence)child).childrenAsList());
            }
        }
        return childrenList;
    }

    public String withColor (Color color) {
        String colorString = null;
        if(Color.GREY.equals(color)) {
            colorString = "grau";
        } else if (Color.RED.equals(color)) {
            colorString = "rot";
        } else if (Color.BLUE.equals(color)) {
            colorString = "blau";
        } else if (Color.GREEN.equals(color)) {
            colorString = "grün";
        } else if (Color.YELLOW.equals(color)) {
            colorString = "gelb";
        }
        return colorString;
    }

    public void setChildren(ArrayList<CodeNode> children) {
        this.children = children;
    }

    public ArrayList<CodeNode> getChildren() {
        return children;
    }
}