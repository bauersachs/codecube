package de.sachsbauer.codecube.codeparts;

import javafx.scene.paint.Color;

public class CodeConditionLoop extends CodeSequence {
    private final boolean negation;
    private final Color color;

    public CodeConditionLoop (CodeSequence parent, boolean negation, Color color) {
        super(parent);
        this.negation = negation;
        this.color = color;
    }

    @Override
    public CodeLeaf getNext() {
        if(checkCondition()) {
            return super.getNext();
        }
        return getParent().getNeighborOf(this);
    }


    @Override
    public CodeLeaf getNeighborOf(CodeNode child) {
        if(getChildren().indexOf(child) + 1 < getChildren().size()) {
            return super.getNeighborOf(child);
        }
        return getNext();
    }

    private boolean checkCondition() {
        return negation ^ (getModel().getCurrentGround() == color);
    }

    @Override
    public String toString() {
        return "solange (" +
                (negation ? "!" : "") +
                withColor(color) +
                ") {\n\t" +
                super.toString().replaceAll("\n", "\n\t") +
                "\n}";
    }

    public String thisToString() {
        return "solange (" +
                (negation ? "!" : "") +
                withColor(color) +
                ")";
    }
}
