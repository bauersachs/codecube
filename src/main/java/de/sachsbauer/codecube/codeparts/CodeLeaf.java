package de.sachsbauer.codecube.codeparts;

public class CodeLeaf extends CodeNode{
    private final Direction direction;

    public CodeLeaf(CodeSequence parent, Direction direction) {
        setParent(parent);
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public CodeLeaf getNext() {
        return getParent().getNeighborOf(this);
    }

    @Override
    public String toString() {
        return direction.toString();
    }

    @Override
    public String thisToString() {
        return toString();
    }

    public void resetNode() {}
}