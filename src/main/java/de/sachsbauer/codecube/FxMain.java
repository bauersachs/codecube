package de.sachsbauer.codecube;

import de.sachsbauer.codecube.parson.Controller;
import de.sachsbauer.codecube.parson.Model;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.beans.Observable;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javafx.util.Duration;
import nu.xom.*;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class FxMain extends Application {
    private double anchorX, anchorY;
    private double anchorAngleX = 0;
    private double anchorAngleY = 0;
    private final DoubleProperty angleX = new SimpleDoubleProperty(0);
    private final DoubleProperty angleY = new SimpleDoubleProperty(0);

    private ArrayList<ModelInterface> models;
    private ArrayList<Parent> rootNodes;
    private ArrayList<AnchorPane> paneList;
    private Stage primaryStage;

    private final int size = 1000;
    private final double zoomRange = 1600;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        // Baue Würfel aus Seiten und Rahmen zusammen
        paneList = createPanes(size);
        this.primaryStage = primaryStage;

        ArrayList<Box> cornerBoxes = createCornerBoxes(size);
        Group cube = new Group();
        cube.getChildren().addAll(paneList);
        cube.getChildren().addAll(cornerBoxes);

        // Setze Ansicht
        Scene sceneMain = new Scene(cube, 800, 800, true, SceneAntialiasing.BALANCED);
        Camera camera = new PerspectiveCamera();
        camera.setTranslateX(size * -.5);
        camera.setTranslateY(size * -.5);
        camera.setTranslateZ(size * -2);
        sceneMain.setCamera(camera);

        // Initialisiere Benutzerkontrolle
        initMouseControl(cube, sceneMain);

        // Zeige Szene
        primaryStage.setScene(sceneMain);
        primaryStage.setFullScreen(true);
        primaryStage.setTitle("CodeCube");
        primaryStage.show();

        // Starte im Menü
        initMenu();
    }

    private void initMenu() {
        BorderPane pane = new BorderPane();
        pane.setPrefSize(size, size);
        Button loadButton = new Button("Szenario laden");
        loadButton.setStyle("-fx-font-size: 10em;");
        pane.setCenter(loadButton);

        paneList.get(0).getChildren().add(pane);
        loadButton.setOnAction(actionEvent -> {
            if(initGame())
                paneList.get(0).getChildren().remove(pane);
        });
    }

    private void resetGame() {
        models = null;
        for(AnchorPane anchorPane : paneList) {
            anchorPane.getChildren().clear();
            anchorPane.setBackground(null);
        }
        rootNodes = null;
        initMenu();
    }

    private boolean initGame() {
        File scenarioFile = chooseScenarioFile(primaryStage);

        if(scenarioFile == null) return false;

        // Starte Spiellogik und -UI
        models = new ArrayList<>(5);
        rootNodes = new ArrayList<>(6);
        if(!loadScenario(scenarioFile)) return false;

        // Timer soll vorne liegen
        for (int i = 1; i < rootNodes.size(); i++) {
            paneList.get(i).getChildren().add(rootNodes.get(i-1));
        }
        paneList.get(0).getChildren().add(rootNodes.get(rootNodes.size()-1));

        // Alle Seiten weiß anmalen!
        for(AnchorPane anchorPane : paneList) {
            anchorPane.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));
        }
        return true;
    }

    private File chooseScenarioFile(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        try {
            URL url = this.getClass().getProtectionDomain().getCodeSource().getLocation();
            File jarPath = new File(url.toURI()).getParentFile();
            fileChooser.setInitialDirectory(jarPath);
        } catch (final URISyntaxException e) {
            e.printStackTrace();
        }

        fileChooser.setTitle("Würfeldatei öffnen");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Würfeldateien","*.xml"));

        return fileChooser.showOpenDialog(primaryStage);
    }

    private boolean loadScenario(File file) {
        try {
            XMLValidator validator = XMLValidator.getInstance();
            validator.validateXMLSchema(getClass().getResource("/scenario.xsd"),file.getPath());
            Builder parser = new Builder();
            Document doc = parser.build(file);
            return parseRoot(doc.getRootElement());
        } catch (SAXException | ParsingException e){
            System.err.println("XML invalide!");
            System.err.println(e.getMessage());
            return false;
        } catch (IOException e) {
            System.err.println("Szenariodatei konnte nicht gelesen werden!");
            System.err.println(e.getMessage());
            return false;
        }
    }

    private boolean parseRoot(Element root) {
        Element timerItem = null;
        for(Element child : root.getChildElements()) {
            switch(child.getQualifiedName()) {
                case "timerItem":
                    timerItem = child;
                    break;
                case "puzzleItem":
                    buildSide(child);
                    break;
                default:
                    System.err.println("Unbekanntes XML Element unter Wurzel:" + child.getQualifiedName());
                    return false;
            }
        }
        if(timerItem != null) {
            buildSide(timerItem);
            return true;
        } else {
            return false;
        }
    }

    private void buildSide (Element el) {
        String paneType = el.getAttribute(0).getValue();

        FXMLLoader loader = new FXMLLoader();
        ModelInterface model;
        loader.setLocation(getClass().getResource("/" + paneType + "/View.fxml"));
        switch (paneType) {
            case "parson":
                model = new Model();
                loader.setControllerFactory(e -> new Controller((Model) model));
                model.initModel(el);
                models.add(model);
                break;
            case "labyrinth":
                model = new de.sachsbauer.codecube.labyrinth.Model();
                loader.setControllerFactory(e -> new de.sachsbauer.codecube.labyrinth.Controller((de.sachsbauer.codecube.labyrinth.Model) model));
                model.initModel(el);
                models.add(model);
                break;
            case "robot":
                model = new de.sachsbauer.codecube.robot.Model();
                loader.setControllerFactory(e -> new de.sachsbauer.codecube.robot.Controller((de.sachsbauer.codecube.robot.Model) model));
                model.initModel(el);
                models.add(model);
                break;
            case "timer":
                model = new de.sachsbauer.codecube.timer.Model(models);
                loader.setControllerFactory(e -> new de.sachsbauer.codecube.timer.Controller((de.sachsbauer.codecube.timer.Model) model));
                model.initModel(el);
                model.solved().addListener(this::cubeWon);
                ((de.sachsbauer.codecube.timer.Model)model).lost().addListener(this::cubeLost);
                break;
        }

        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        rootNodes.add(root);
    }

    private void cubeWon(Observable observable) {
        for(ModelInterface model : models) {
            model.uiLocked().set(true);
        }

        for(int i = 0; i < paneList.size(); i++) {
            AnchorPane anchorPane = paneList.get(i);
            anchorPane.setBackground(new Background(new BackgroundFill(Color.GREEN, null, null)));
            if(anchorPane.getChildren().size() > 0) {
                FadeTransition ft = new FadeTransition(Duration.millis(3000), anchorPane.getChildren().get(0));
                ft.setFromValue(1);
                ft.setToValue(0);
                ft.play();
                if(i == 0) {
                    ft.setOnFinished(actionEvent -> resetGame());
                }
            }
        }
    }

    private void cubeLost(Observable observable, Boolean aBoolean, Boolean t1) {
        for(ModelInterface model : models) {
            model.uiLocked().set(true);
        }

        for(int i = 0; i < paneList.size(); i++) {
            AnchorPane anchorPane = paneList.get(i);
            anchorPane.setBackground(new Background(new BackgroundFill(Color.RED, null, null)));
            if(anchorPane.getChildren().size() > 0) {
                FadeTransition ft = new FadeTransition(Duration.millis(3000), anchorPane.getChildren().get(0));
                ft.setFromValue(1);
                ft.setToValue(0);
                ft.play();
                if(i == 0) {
                    ft.setOnFinished(actionEvent -> resetGame());
                }
            }
        }
    }

    private ArrayList<AnchorPane> createPanes(int size) {
        ArrayList<AnchorPane> paneList = new ArrayList<>(6);
        for(int i = 0; i < 6; i++) {
            AnchorPane anchor = new AnchorPane();
            anchor.setPrefWidth(size);
            anchor.setPrefHeight(size);
            switch (i) {
                // Front
                case 0:
                    anchor.setTranslateX(-0.5 * size);
                    anchor.setTranslateY(-0.5 * size);
                    anchor.setTranslateZ(-0.5 * size);
                    break;
                // Back
                case 1:
                    anchor.setTranslateX(-0.5 * size);
                    anchor.setTranslateY(-0.5 * size);
                    anchor.setTranslateZ(0.5 * size);
                    anchor.setRotationAxis(Rotate.Y_AXIS);
                    anchor.setRotate(180);
                    break;
                // Left
                case 2:
                    anchor.setTranslateX(-1 * size);
                    anchor.setTranslateY(-0.5 * size);
                    anchor.setTranslateZ(0);
                    anchor.setRotationAxis(Rotate.Y_AXIS);
                    anchor.setRotate(90);
                    break;
                // Right
                case 3:
                    anchor.setTranslateX(0);
                    anchor.setTranslateY(-0.5 * size);
                    anchor.setTranslateZ(0);
                    anchor.setRotationAxis(Rotate.Y_AXIS);
                    anchor.setRotate(-90);
                    break;
                // Top    
                case 4:
                    anchor.setTranslateX(-0.5 * size);
                    anchor.setTranslateY(-1 * size);
                    anchor.setTranslateZ(0);
                    anchor.setRotationAxis(Rotate.X_AXIS);
                    anchor.setRotate(-90);
                    break;
                // Bottom
                case 5:
                    anchor.setTranslateX(-0.5 * size);
                    anchor.setTranslateY(0);
                    anchor.setTranslateZ(0);
                    anchor.setRotationAxis(Rotate.X_AXIS);
                    anchor.setRotate(90);
                    break;
            }
            paneList.add(anchor);
        }
        return paneList;
    }

    private ArrayList<Box> createCornerBoxes(int size) {
        ArrayList<Box> cornerBoxes = new ArrayList<>(12);

        for(int i = 0; i < 12; i++) {
            Box box = new Box(1.2* size, 0.102 * size, 0.102 *size);
            box.setMaterial(new PhongMaterial(Color.DARKRED));
            switch (i) {
                case 0:
                    box.setTranslateY(0.55*size);
                    box.setTranslateZ(-0.55*size);
                    break;
                case 1:
                    box.setTranslateY(-0.55*size);
                    box.setTranslateZ(-0.55*size);
                    break;
                case 2:
                    box.setRotationAxis(Rotate.Z_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(0.55*size);
                    box.setTranslateZ(-0.55*size);
                    break;
                case 3:
                    box.setRotationAxis(Rotate.Z_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(-0.55*size);
                    box.setTranslateZ(-0.55*size);
                    break;
                case 4:
                    box.setTranslateY(0.55*size);
                    box.setTranslateZ(0.55*size);
                    break;
                case 5:
                    box.setTranslateY(-0.55*size);
                    box.setTranslateZ(0.55*size);
                    break;
                case 6:
                    box.setRotationAxis(Rotate.Z_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(0.55*size);
                    box.setTranslateZ(0.55*size);
                    break;
                case 7:
                    box.setRotationAxis(Rotate.Z_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(-0.55*size);
                    box.setTranslateZ(0.55*size);
                    break;
                case 8:
                    box.setRotationAxis(Rotate.Y_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(0.55*size);
                    box.setTranslateY(-0.55*size);
                    break;
                case 9:
                    box.setRotationAxis(Rotate.Y_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(0.55*size);
                    box.setTranslateY(0.55*size);
                    break;
                case 10:
                    box.setRotationAxis(Rotate.Y_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(-0.55*size);
                    box.setTranslateY(0.55*size);
                    break;
                case 11:
                    box.setRotationAxis(Rotate.Y_AXIS);
                    box.setRotate(90);
                    box.setTranslateX(-0.55*size);
                    box.setTranslateY(-0.55*size);
                    break;
                default:
            }
            cornerBoxes.add(box);
        }
        return cornerBoxes;
    }

    private void initMouseControl(Group group, Scene scene) {
        Rotate xRotate;
        Rotate yRotate;
        group.getTransforms().addAll(
                xRotate = new Rotate(0, Rotate.X_AXIS),
                yRotate = new Rotate(0, Rotate.Y_AXIS)
        );
        xRotate.angleProperty().bind(angleX);
        yRotate.angleProperty().bind(angleY);

        for (Node node : group.getChildren()) {
            if(node instanceof Box) {
                node.setOnMousePressed(event -> {
                    anchorX = event.getSceneX();
                    anchorY = event.getSceneY();
                    anchorAngleX = angleX.get();
                    anchorAngleY = angleY.get();
                });

                node.setOnMouseDragged(event -> {
                    angleX.set(anchorAngleX - (anchorY - event.getSceneY()));
                    angleY.set(anchorAngleY + anchorX - event.getSceneX());
                });
            }
        }

        scene.setOnScroll(event -> {
                    double value = group.getTranslateZ() - 10 * event.getDeltaY();
                    group.setTranslateZ(Math.max(Math.min(value, zoomRange), -zoomRange));
                }
        );
    }
}