package de.sachsbauer.codecube.parson;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import de.sachsbauer.codecube.ModelInterface;
import nu.xom.Element;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 * Model der Parsons Puzzles.
 * @author Daniel Bauersachs
 */
public class Model implements ModelInterface {
    private final ArrayList<ArrayList<CodeFragment>> codeList;
    private final ArrayList<CodeFragment> correctCodeList;
    private final PropertyChangeSupport changes = new PropertyChangeSupport(this);
    private final BooleanProperty solved;
    private final StringProperty title;
    private int maxIndentation;
    private final BooleanProperty uiLocked;

    public Model() {
        uiLocked = new SimpleBooleanProperty(false);
        title = new SimpleStringProperty();
        maxIndentation = 10;
        codeList = new ArrayList<>();
        correctCodeList = new ArrayList<>();
        solved = new SimpleBooleanProperty(false);
        changes.addPropertyChangeListener(evt -> solved.set(isSolved()));
    }

    public void initModel(Element root) {
        String text;
        int indentation;
        codeList.add(new ArrayList<>());

        for(Element el : root.getChildElements()) {
            String elementName = el.getQualifiedName();
            switch (elementName) {
                case "puzzleTitle":
                    title.setValue(el.getValue());
                    break;
                case "tupleItems":
                    text = el.getChildElements("text").get(0).getValue();
                    indentation = Integer.parseInt(el.getChildElements("indentation").get(0).getValue());
                    codeList.get(0).add(new CodeFragment(text, indentation));
                    correctCodeList.add(new CodeFragment(text, indentation));
                    break;
            }
        }
        Collections.shuffle(codeList.get(0));

        for (int i = 1; i <= maxIndentation; i++) {
            codeList.add(new ArrayList<>());
            for (int k = 0; k < getCodeSize(); k++) {
                codeList.get(i).add(null);
            }
        }
        computeMaxIndentation();
    }

    private void computeMaxIndentation() {
        int maxIndentation = 0;
        for (ArrayList<CodeFragment> a : codeList) {
            for (CodeFragment f : a) {
                if (f != null && f.getIndentation() > maxIndentation)
                    maxIndentation = f.getIndentation();
            }
        }
        this.maxIndentation = maxIndentation + 1;
    }

    public void setFragmentAt(int x, int y, CodeFragment fragment) {
        if (x >= 0 && x <= maxIndentation) {
            codeList.get(x).set(y, fragment);
            changes.firePropertyChange("codeArray", null, null);
        }
    }

    public CodeFragment getFragmentAt(int x, int y) {
        if (x >= 0 && x <= maxIndentation && y >= 0 && y <= getCodeSize()) {
            return codeList.get(x).get(y);
        }
        return null;
    }

    /**
     * Verschiebt ein {@link CodeFragment} an einen anderen Platz. Dabei wird auch, falls nötig,
     * ein anderes Fragment in der Zielreihe verschoben sowie die erste Reihe gestutzt.
     * @param sourceX Quellabszisse
     * @param sourceY Quellordinate
     * @param targetX Zielabszisse
     * @param targetY Zielordinate
     */
    public void moveFragment(int sourceX, int sourceY, int targetX, int targetY) {
        if (targetX > 0 && (sourceX == 0 || sourceY != targetY)) {
            for (int x = 1; x <= getMaxIndentation(); x++) {
                if (getFragmentAt(x, targetY) != null && targetX != x) {
                    swapFragments(targetX, targetY, x, targetY);
                    x = getMaxIndentation();
                }
            }
        }
        swapFragments(sourceX, sourceY, targetX, targetY);
        trimFirstRow();
    }

    public void swapFragments(int sourceX, int sourceY, int targetX, int targetY) {
        CodeFragment temp = getFragmentAt(targetX, targetY);
        setFragmentAt(targetX, targetY, getFragmentAt(sourceX, sourceY));
        setFragmentAt(sourceX, sourceY, temp);
    }

    public void reset () {
        int codeSize = getCodeSize();
        for (int i = 1; i <= maxIndentation; i++) {
            for (int k = 0; k < codeSize; k++) {
                CodeFragment f = codeList.get(i).get(k);
                if(f != null) {
                    codeList.get(0).add(f);
                    codeList.get(i).set(k, null);
                }
            }
        }
        codeList.get(0).removeIf(Objects::isNull);
        Collections.shuffle(codeList.get(0));
        changes.firePropertyChange("codeArray", null, null);
    }

    public void trimFirstRow() {
        int initialCodeSize = getCodeSize();
        codeList.get(0).removeIf(Objects::isNull);
        initialCodeSize -= getCodeSize();
        for (int i = 0; i < initialCodeSize; i++) {
            codeList.get(0).add(null);
        }
        changes.firePropertyChange("codeArray", null, null);
    }

    public int getCodeSize() {
        if (codeList.get(0) != null)
            return codeList.get(0).size();
        return 0;
    }

    /**
     * @return Des Rätsels Zustand
     */
    public boolean isSolved() {
        // Noch Fragmente im Vorrat?
        for(int y = 0; y < getCodeSize(); y++) {
            if (getFragmentAt(0, y) != null) {
                return false;
            }
        }

        // Elemente an korrekter Position?
        for (int x = 1; x <= maxIndentation; x++) {
            for (int y = 0; y < getCodeSize(); y++) {
                CodeFragment f = getFragmentAt(x, y);
                if (f != null
                        && !(correctCodeList.get(y).getText().equals(f.getText())
                                && (x - 1) == correctCodeList.get(y).getIndentation()))
                    return false;
            }
        }
        uiLocked.set(true);
        return true;
    }

    public int getMaxIndentation() {
        return maxIndentation;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        changes.addPropertyChangeListener(propertyChangeListener);
    }

    public BooleanProperty solved() {
        return solved;
    }

    public BooleanProperty uiLocked() {
        return uiLocked;
    }

    public StringProperty title() {
        return title;
    }
}