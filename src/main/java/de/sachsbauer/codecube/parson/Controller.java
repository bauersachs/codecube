package de.sachsbauer.codecube.parson;

import javafx.beans.property.BooleanProperty;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller des Parson's Puzzle. Kommunikation mit Model und View per Attribut bzw. FXML
 * @author Daniel Bauersachs
 */
public class Controller implements Initializable, PropertyChangeListener {
    private final Model model;

    private Label sourceLabel = null;
    private Label targetLabel = null;

    @FXML
    private VBox vBoxGrid = null;

    @FXML
    private Button resetButton;

    @FXML
    private Text title;

    @FXML
    private Circle solvedLight;

    public Controller(Model model) {
        this.model = model;
    }

    /**
     * Wird nach dem Konstruieren des Controllers aufgerufen und erstelllt die Label mit
     * ihrem jeweiligen {@link javafx.event.EventHandler}
     * @param location Nicht genutzt
     * @param resources Nicht genutzt
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialisieren des Titels
        title.textProperty().bind(model.title());

        // Initialisieren der Anzeige der Codefragmente
        PseudoClass dragOver = PseudoClass.getPseudoClass("drag-over");
        for (int i = 0; i < model.getCodeSize(); i++) {
            vBoxGrid.getChildren().add(new HBox());
            for (int k = 0; k <= model.getMaxIndentation(); k++) {
                Label label = new Label();
                label.disableProperty().bind(model.uiLocked());
                label.setOnDragDetected(this::dragDetected);
                label.setOnDragOver(this::dragOver);
                label.setOnDragDropped(this::dragDropped);
                label.setOnDragDone(this::dragDone);

                label.setOnDragEntered(e -> label.pseudoClassStateChanged(dragOver, true));
                label.setOnDragExited(e -> label.pseudoClassStateChanged(dragOver, false));
                if(k == 0) {
                    label.getStyleClass().add("label-first-column");
                }
                ((HBox) vBoxGrid.getChildren().get(i)).getChildren().add(label);
            }
        }

        // Initialisieren des Buttons
        resetButton.setOnAction(this :: resetButtonPressed);
        resetButton.disableProperty().bind(model.uiLocked());

        model.addPropertyChangeListener(this);
        propertyChange(null);
        addChangeListener();
    }

    private void resetButtonPressed (ActionEvent event) {
        model.reset();
    }

    private void dragDetected(MouseEvent event)
    {
        sourceLabel = (Label) event.getSource();
        String sourceString = sourceLabel.getText();
        if (sourceString == null || sourceString.trim().equals("")) {
            event.consume();
            return;
        }

        Dragboard dragboard = sourceLabel.startDragAndDrop(TransferMode.COPY_OR_MOVE);
        ClipboardContent content = new ClipboardContent();
        content.putString(sourceString);
        dragboard.setContent(content);
        event.consume();
    }

    private void dragOver(DragEvent event)
    {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasString()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    private void dragDropped(DragEvent event)
    {
        Dragboard dragboard = event.getDragboard();
        if (dragboard.hasString()) {
            targetLabel = (Label) event.getSource();
            event.setDropCompleted(true);
        }
        else {
            event.setDropCompleted(false);
        }
        event.consume();
    }

    private void dragDone(DragEvent event)
    {
        TransferMode modeUsed = event.getTransferMode();
        if (modeUsed == TransferMode.MOVE) {
            int sourceX = getX(sourceLabel);
            int sourceY = getY(sourceLabel);
            int targetX = getX(targetLabel);
            int targetY = getY(targetLabel);
            if(sourceX == targetX && sourceY == targetY && sourceX > 0 && targetX < model.getMaxIndentation()) {
                model.moveFragment(sourceX, sourceY, sourceX + 1, sourceY);
            } else {
                model.moveFragment(sourceX, sourceY, targetX, targetY);
            }
        }
        event.consume();
    }

    /**
     * Sucht die Abszisse eines Labels
     * @param label Gesuchtes Label
     * @return Abszisse des Labels, -1 wenn nicht gefunden
     */
    private int getX(Label label) {
        for (Node node : vBoxGrid.getChildren()) {
            if(node instanceof HBox) {
                int x = ((HBox)node).getChildren().indexOf(label);
                if (x >= 0) return x;
            }
        }
        return -1;
    }

    /**
     * Sucht die Ordinate eines Labels
     * @param label Gesuchtes Label
     * @return Ordinate des Labels, -1 wenn nicht gefunden
     */
    private int getY(Label label) {
        for (Node node : vBoxGrid.getChildren()) {
            if(node instanceof HBox) {
                if(((HBox) node).getChildren().contains(label)) {
                    int y = vBoxGrid.getChildren().indexOf(node);
                    if (y >= 0) return y;
                }
            }
        }
        return -1;
    }

    /**
     * EventHandler, wenn Fragmente verschoben werden
     * @param evt Nicht genutzt
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        for (Node node : vBoxGrid.getChildren()) {
            for(Node node2 : ((HBox) node).getChildren()) {
                Label label = (Label) node2;
                int x = getX(label);
                int y = getY(label);
                CodeFragment f = model.getFragmentAt(x, y);
                label.setText(f == null ? "" : f.getText());
            }
        }
    }

    /**
     * Fügt einen ChangeListener für die Lampen hinzu.
     * Dieser sorgt dafür, dass die Lampen rot bei ungelöstem und grün bei gelöstem Rätsel sind.
     * @author Daniel Bauersachs
     */
    private void addChangeListener () {
        BooleanProperty b = model.solved();
        b.addListener( (observableValue, aBoolean, t1) ->
                solvedLight.setStyle("-fx-fill: " + (t1 ? "green" : "red")));
    }
}