package de.sachsbauer.codecube.parson;

/**
 * Hält eine Zeile Code mit korrekter Einschubweite
 * @author Daniel Bauersachs
 */
public class CodeFragment {
    private final String text;
    private final int indentation;

    public CodeFragment (String text, int indentation) {
        this.text = text;
        this.indentation = indentation;
    }

    public int getIndentation() {
        return indentation;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString () {
        return "\t".repeat(Math.max(0, indentation)) + text;
    }
}