package de.sachsbauer.codecube.timer;

import javafx.beans.binding.*;
import javafx.beans.property.*;
import de.sachsbauer.codecube.ModelInterface;
import nu.xom.Element;

import java.util.ArrayList;

public class Model implements ModelInterface{
    private final StringProperty title;
    private Integer STARTTIME;

    private final IntegerProperty timeSeconds = new SimpleIntegerProperty(1);
    private final NumberBinding minutesFormatted = timeSeconds.divide(60);
    private final NumberBinding secondsFormatted = timeSeconds.subtract(minutesFormatted.multiply(60));
    private final StringExpression timeFormatted = Bindings.format("%02d:%02d", minutesFormatted, secondsFormatted);

    private final ArrayList<BooleanProperty> solvedPuzzles;

    private final BooleanProperty solved;
    private final BooleanProperty lost;
    private final BooleanProperty uiLocked;

    public Model (ArrayList<ModelInterface> models) {
        solved = new SimpleBooleanProperty(false);
        lost = new SimpleBooleanProperty(false);
        lost.bind(solved().not().and(timeSeconds.isEqualTo(0)));

        uiLocked = new SimpleBooleanProperty(false);
        title = new SimpleStringProperty();
        solvedPuzzles = new ArrayList<>(5);

        for(ModelInterface model : models) {
            model.solved().addListener((observableValue, aBoolean, t1) -> computeSolved());
            solvedPuzzles.add(model.solved());
        }
    }

    public ArrayList<BooleanProperty> getSolvedPuzzles() {
        return solvedPuzzles;
    }

    public IntegerProperty timeSecondsProperty() {
        return timeSeconds;
    }

    public void setTimeSeconds(int timeSeconds) {
        this.timeSeconds.set(timeSeconds);
    }

    public Integer getSTARTTIME() {
        return STARTTIME;
    }

    public StringExpression timeFormatted() {
        return timeFormatted;
    }

    @Override
    public void initModel(Element root) {
        for(Element el : root.getChildElements()) {
            String elementName = el.getQualifiedName();
            switch(elementName) {
                case "puzzleTitle":
                    title.set(el.getValue());
                    break;
                case "solvingTime":
                    STARTTIME = Integer.parseInt(el.getValue());
                    setTimeSeconds(STARTTIME);
                    break;
            }
        }
    }

    @Override
    public BooleanProperty solved() {
        return solved;
    }

    private void computeSolved() {
        for(BooleanProperty solvedPuzzle : solvedPuzzles) {
            if(!solvedPuzzle.get()) {
                solved.set(false);
                return;
            }
        }
        solved.set(true);
    }

    public BooleanProperty lost() {
        return lost;
    }

    public BooleanProperty uiLocked() {
        return uiLocked;
    }

    public StringProperty title() { return title; }
}