package de.sachsbauer.codecube.timer;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller des Timers
 * @author Daniel Bauersachs
 */
public class Controller implements Initializable {
    private final Model model;

    @FXML
    private Label timer;

    @FXML
    private HBox lightBox;

    @FXML
    private Text title;

    public Controller (Model model) {
        this.model = model;
    }

    /**
     * Initialisiert die View, setzt EventHandler.
     * @param url Nicht genutzt.
     * @param resourceBundle Nicht genutzt.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        timer.textProperty().bind(model.timeFormatted());
        title.textProperty().bind(model.title());
        for(int i = 0; i < model.getSolvedPuzzles().size(); i++) {
            lightBox.getChildren().get(i).setStyle("-fx-fill: red");
        }
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(model.getSTARTTIME() + 1),
                new KeyValue(model.timeSecondsProperty(), 0)));
        timeline.playFromStart();

        addChangeListener();
    }

    /**
     * Fügt einen ChangeListener für die Lampen hinzu.
     * Dieser sorgt dafür, dass die Lampen rot bei ungelöstem und grün bei gelöstem Rätsel sind.
     */
    private void addChangeListener () {
        for(int i = 0; i < model.getSolvedPuzzles().size(); i++) {
            BooleanProperty b = model.getSolvedPuzzles().get(i);
            Node node = lightBox.getChildren().get(i);
            b.addListener((observableValue, aBoolean, t1) ->
                    node.setStyle("-fx-fill: " + (t1 ? "green" : "red")));
        }
    }
}